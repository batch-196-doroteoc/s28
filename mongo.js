// db.users.insertOne({
// 
//     
// 
//     "username": "dahyunTwice",
// 
//     "password": "dahyunKim" 
// 
//  
// 
//     })    

// db.users.insertOne({
// 
//     
// 
//     "username": "gokuSon",
// 
//     "password": "over9000" 
// 
//  
// 
//     })    

//insert multiple documents at once

// db.users.insertMany(
// 
//     [
//         
//         {
//           "username":"pablo123",
//           "password":"123paul"
//         },
//         {
//           "username":"pedro99",
//           "password": "iampeter99"
//             
//         }  
//     ]
// )
        
// db.products.insertMany(
// 
//     [
//         
//         {
//           "name":"Chickenjoy with rice",
//           "description":"Juicylicious, Crispylicious",
//           "price": 76
//         },
//         {
//           "name":"Cheesy Yumburger",
//           "description": "Langhap Sarap",
//           "price": 49
//         },
//         {
//           "name":"Large Peach Mango Pie",
//           "description": "Nakakapaso",
//           "price": 45
//         }
//       
//         
//     ]
// )
        
// db.products.insertMany(
// 
//     [
//         
//         {
//           "name":"Nuclear Reactor",
//           "description":"blue, slightly used",
//           "price": 760000000
//         },
//         {
//           "name":"Atomic Bomb 3 pcs.",
//           "description": "Still works",
//           "price": 49000000
//         },
//         {
//           "name":"Flamethrower",
//           "description": "Nakakapaso",
//           "price": 4500000000
//         }
//       
//         
//     ]
// )       

//read/retrieve
//db.collection.find() - return/find all documents in the collection
// db.users.find()

//db.collection.find({"criteria":"value"}) - returns/finds all documents that match the criteria
// db.users.find({"username":"pedro99"})

// db.cars.insertMany(
//     [
//         {
//             "name":"Vios",
//             "brand":"Toyota",
//             "type": "sedan",
//             "price": 1500000
//         },
//         {
//             "name":"Tamaraw FX",
//             "brand":"Toyota",
//             "type": "auv",
//             "price": 750000
//         },
//         {
//             "name":"City",
//             "brand":"Honda",
//             "type": "sedan",
//             "price": 1600000
//         }
//     ])

// db.cars.find({"type":"sedan"})
// db.cars.find({"brand":"Toyota"})

//db.collection.findOne({}) - find/return the first item/document in the collection
// db.cars.findOne({})

//db.collection.findOne({"criteria":"value"}) - find/return
//the first item/docu that matches the criteria
// db.cars.findOne({"type":"sedan"})

// db.cars.findOne({"brand":"Toyota"})
//db.cars.findOne({"brand":"Honda"})

//update
//db.collection.updateOne({"criteria":"value"},{$set:{"fieldToBeUpdated":"updatedValue"}})
//Allows us to update the first item that matches our criteria
// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter1999"}})

//db.collection.updateOne({},{$set:{"fieldToBeUpdated":"Updated Value"}})
//allows us to update the first item in  the collection
// db.users.updateOne({},{$set:{"username":"updateUsername"}})

//if the field being updated does not yet exist, mongodb will instead add that field into the document
//db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin":true}})

//db.collection.updateMany({},{$set:{"fieldToUpdated":"Updated Value}})
//allows us to update all items in the collection.
//db.users.updateMany({},{$set:{"isAdmin":true}})

//db.collection.updateMany({"c":"v"},{$set:{"fieldUpd":"Upd v"}})
//allows us to update all items that matches our criteria
// db.cars.updateMany({"type":"sedan"},{$set:{"price":1000000}})

//delete
//db.collection.deleteOne({}) - deletes the first item in collection
// db.products.deleteOne({})

//db.collection.deleteOne({"c":"v"})
//deletes the first item that matches the criteria
// db.cars.deleteOne({"brand":"Toyota"})

//db.collection.deleteMany({"c":"v"})
//deletes all items that matches the criteria
//db.users.deleteMany({"isAdmin":true})

///db.collection.deleteMany({})
//delete all documents in a collection
//db.products.deleteMany({})
// db.cars.deleteMany({})



